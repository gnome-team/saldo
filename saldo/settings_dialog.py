# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Adw, Gio, Gtk

import saldo.config_manager as config
from saldo.const import APP_ID


@Gtk.Template(resource_path="/org/tabos/saldo/ui/settings_dialog.ui")
class SettingsDialog(Adw.PreferencesWindow):
    __gtype_name__ = "SettingsDialog"

    _general_group = Gtk.Template.Child()
    _dark_theme_row = Gtk.Template.Child()
    _lock_timer_spin_button = Gtk.Template.Child()
    _safe_days_spin_button = Gtk.Template.Child()
    _automatic_refresh_switch = Gtk.Template.Child()

    def __init__(self, window):
        super().__init__()

        self.window = window
        self.props.transient_for = self.window
        self._set_config_values()

    def on_dark_theme(self, action, *args):  # pylint: disable=unused-argument
        manager = Adw.StyleManager.get_default()
        if action.props.state:
            manager.props.color_scheme = Adw.ColorScheme.PREFER_DARK
        else:
            manager.props.color_scheme = Adw.ColorScheme.DEFAULT

    def on_lock_timer_changed(self, val):
        config.set_lock_timer_seconds(val.get_value_as_int())

    def on_safe_days_changed(self, val):
        config.set_safe_days(val.get_value_as_int())

    def _set_config_values(self):
        action_group = Gio.SimpleActionGroup.new()
        settings = Gio.Settings.new(APP_ID)

        # General
        manager = Adw.StyleManager.get_default()
        if not manager.props.system_supports_color_schemes:
            self._general_group.props.visible = True
            dark_mode_action = settings.create_action("dark-theme")
            action_group.add_action(dark_mode_action)
            dark_mode_action.connect("notify::state", self.on_dark_theme)

        settings.bind(
            "lock-timer",
            self._lock_timer_spin_button,
            "value",
            Gio.SettingsBindFlags.DEFAULT,
        )
        self._lock_timer_spin_button.connect(
            "value-changed", self.on_lock_timer_changed
        )

        settings.bind(
            "safe-days",
            self._safe_days_spin_button,
            "value",
            Gio.SettingsBindFlags.DEFAULT,
        )
        self._safe_days_spin_button.connect("value-changed", self.on_safe_days_changed)

        settings.bind(
            "automatic-refresh",
            self._automatic_refresh_switch,
            "active",
            Gio.SettingsBindFlags.DEFAULT,
        )
        self._automatic_refresh_switch.connect("state-set", self._on_automatic_refresh)

        quickunlock_action = settings.create_action("quickunlock")
        action_group.add_action(quickunlock_action)
        fingerprint_quickunlock_action = settings.create_action(
            "fingerprint-quickunlock",
        )
        action_group.add_action(fingerprint_quickunlock_action)

        self.insert_action_group("settings", action_group)

    def _on_automatic_refresh(self, action, *args):  # pylint: disable=unused-argument
        if not action.props.state:
            self.window.start_automatic_refresh()
        else:
            self.window.stop_automatic_refresh()
