# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Adw, Gtk


@Gtk.Template(resource_path="/org/tabos/saldo/ui/create_safe_page.ui")
class CreateSafePage(Adw.Bin):
    __gtype_name__ = "CreateSafePage"

    _safe_password1 = Gtk.Template.Child()
    _safe_password2 = Gtk.Template.Child()
    _save_safe_password = Gtk.Template.Child()

    def __init__(self, window, backend):
        super().__init__()
        self._window = window
        self._backend = backend

    @Gtk.Template.Callback()
    def _on_safe_password_entry_changed(self, _):
        password1 = self._safe_password1.get_text()

        if password1 != "" and password1 == self._safe_password2.get_text():
            self._save_safe_password.set_sensitive(True)
        else:
            self._save_safe_password.set_sensitive(False)

    @Gtk.Template.Callback()
    def _on_safe_password_button_clicked(self, _):
        self._backend.safe_password = self._safe_password1.get_text()
        self._window.view = self._window.View.LOCKED

    @Gtk.Template.Callback()
    def _on_safe_password_activate(self, _):
        if self._save_safe_password.get_sensitive() is True:
            self._save_safe_password.activate()
