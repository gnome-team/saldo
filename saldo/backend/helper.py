# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gettext import gettext as _
from datetime import datetime, timedelta
import locale


def money(value):
    try:
        return str(locale.currency(value, grouping=True, symbol=False))
    except ValueError:
        return str(value)


def readable_date(date: datetime) -> str:
    today = datetime.today()
    yesterday = today - timedelta(days=1)
    delta = today - date

    if date.date() == today.date():
        date_str = _("Today")
    elif date.date() == yesterday.date():
        date_str = _("Yesterday")
    elif delta.days < 0:
        date_str = _("Open Booking Date:") + " " + date.strftime("%Y-%m-%d")
    else:
        date_str = date.strftime("%A, %B %-d")

    return date_str
