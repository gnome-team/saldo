# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the banking package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: banking\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-13 18:39+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/org.tabos.banking.desktop.in:3 src/resources/ui/window.ui:255
msgid "Banking"
msgstr ""

#: data/org.tabos.banking.appdata.xml.in:6
msgid "FinTS online banking application designed for Linux smartphones."
msgstr ""

#: data/org.tabos.banking.gschema.xml:6
msgid "BLZ"
msgstr ""

#: data/org.tabos.banking.gschema.xml:7
msgid "BLZ."
msgstr ""

#: data/org.tabos.banking.gschema.xml:11
msgid "User"
msgstr ""

#: data/org.tabos.banking.gschema.xml:12
msgid "User name which is used to login to online banking."
msgstr ""

#: data/org.tabos.banking.gschema.xml:16
msgid "Online banking server"
msgstr ""

#: data/org.tabos.banking.gschema.xml:17
msgid "FINTS online banking server url."
msgstr ""

#: src/resources/ui/assistant.ui:28
msgid "BLZ:"
msgstr ""

#: src/resources/ui/assistant.ui:41
msgid "User:"
msgstr ""

#: src/resources/ui/assistant.ui:76
msgid "Login data"
msgstr ""

#: src/resources/ui/assistant.ui:94
msgid "Server:"
msgstr ""

#: src/resources/ui/assistant.ui:130
msgid "Password:"
msgstr ""

#: src/resources/ui/assistant.ui:155 src/resources/ui/window.ui:32
msgid "Assistant"
msgstr ""

#: src/resources/ui/assistant.ui:158
msgid "Done"
msgstr ""

#: src/resources/ui/assistant.ui:172
msgid "Quit"
msgstr ""

#: src/resources/ui/window.ui:45
msgid "Refresh"
msgstr ""

#: src/resources/ui/window.ui:70
msgid "About"
msgstr ""

#: src/resources/ui/window.ui:101
msgid "MY ACCOUNTS --- "
msgstr ""

#: src/resources/ui/window.ui:116
msgid "12.345,67 €"
msgstr ""

#: src/window.py:173
msgid "MY ACCOUNTS"
msgstr ""
